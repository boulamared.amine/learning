import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BooksListComponent } from './components/books-list/books-list.component';
import { BookCreateComponent } from './components/book-create/book-create.component';
import { NotFoundComponent } from './components/not-found/not-found.component';

const routes: Routes = [
  { path : '', redirectTo : '/list' , pathMatch : 'full'},
  { path : 'list', component : BooksListComponent },
  { path : 'create', component: BookCreateComponent },
  { path : '**', component : NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
